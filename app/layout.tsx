import type { Metadata } from "next";
import { Raleway } from "next/font/google";
import "./globals.css";

const font = Raleway({ subsets: ["cyrillic"] });

export const metadata: Metadata = {
 title: "Temen Curhat",
 description: "Develop by Patar",
};

export default function RootLayout({
 children,
}: Readonly<{
 children: React.ReactNode;
}>) {
 return (
  <html lang='en'>
   <body className={font.className}>{children}</body>
  </html>
 );
}
