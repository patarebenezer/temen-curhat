"use client";
import Header from "./LandingPage/components/Header";
import Footer from "./LandingPage/components/Footer";
import Banner from "./LandingPage/Banner";
import DetailBanner from "./LandingPage/DetailBanner";

export default function LandingPage() {
 return (
  <>
   <Header />
   <Banner />
   <DetailBanner />
   <Footer />
  </>
 );
}
