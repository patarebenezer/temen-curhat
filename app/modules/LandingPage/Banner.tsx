"use client";
import Image from "next/image";
import BodyImage from "@/assets/img/black.jpeg";
import MobileCenter from "@/assets/img/tengah.png";
import MobileLeft from "@/assets/img/kiri.png";
import MobileRight from "@/assets/img/kanan.png";
import Round from "./components/Round";

export default function Banner() {
 return (
  <>
   <div className='relative'>
    <Image
     src={BodyImage}
     alt='body'
     className='w-full h-[600px] object-cover object-center'
    />
    <Round type='Top' />
    <Round type='Bottom' />
    <div>
     <Image
      src={MobileCenter}
      alt='mobile center'
      className='w-[300px] absolute bottom-0 left-1/2 transform -translate-x-1/2'
     />
    </div>
   </div>
   <div className='text-white font-bold absolute top-28 left-1/2 transform -translate-x-1/2 text-center lg:text-3xl text-2xl w-full xl:text-4xl'>
    <p>
     Merasa{" "}
     <span className='relative'>
      Kesepian
      <span className='absolute w-full z-[-10] bg-yellow-800 md:bg-yellow-600 lg:bg-yellow-500 left-0 bottom-[6px] !h-2'></span>
     </span>
    </p>
    <p>Curhat Penuh Privacy?</p>
    <p>
     <span className='relative'>
      Baca Alkitab
      <span className='absolute w-full z-[-10] bg-yellow-800 md:bg-yellow-600 lg:bg-yellow-500 left-0 bottom-[6px] !h-2'></span>
     </span>{" "}
     Aja!
    </p>
   </div>
   <Image
    src={MobileLeft}
    alt='mobile left'
    className='w-[260px] hidden md:hidden lg:block lg:absolute bottom-0 left-40 top-24'
   />
   <Image
    src={MobileRight}
    alt='mobile right'
    className='w-[260px] hidden md:hidden lg:block lg:absolute bottom-0 right-40 top-24'
   />
  </>
 );
}
