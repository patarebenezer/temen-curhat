import { useState } from "react";

type MenuProps = {
 type?: string;
};

export default function Menu({ type }: MenuProps) {
 const [activeLink, setActiveLink] = useState("portfolio");

 const handleClick = (link: string) => {
  setActiveLink(link);
 };

 return (
  <div
   className={`flex-wrap text-justify ${
    type === "Footer"
     ? "text-white grid grid-cols-2 gap-x-8 lg:gap-x-12 gap-y-2 lg:gap-y-3 ml-14 my-2 lg:my-5"
     : "text-black flex gap-7"
   }`}
  >
   <a
    href='#'
    className={
     type !== "Footer" && activeLink === "home" ? "border-b-2 border-black" : ""
    }
    onClick={() => handleClick("home")}
   >
    home
   </a>
   <a
    href='#'
    className={activeLink === "about us" ? "border-b-2 border-black" : ""}
    onClick={() => handleClick("about us")}
   >
    about us
   </a>
   <a
    href='#'
    className={
     type !== "Footer" && activeLink === "why us"
      ? "border-b-2 border-black"
      : ""
    }
    onClick={() => handleClick("why us")}
   >
    why us
   </a>
   <a
    href='#'
    className={
     type !== "Footer" && activeLink === "our service"
      ? "border-b-2 border-black"
      : ""
    }
    onClick={() => handleClick("our service")}
   >
    our service
   </a>
   <a
    href='#'
    className={
     type !== "Footer" && activeLink === "portfolio"
      ? "border-b-2 border-black"
      : ""
    }
    onClick={() => handleClick("portfolio")}
   >
    portfolio
   </a>
  </div>
 );
}
