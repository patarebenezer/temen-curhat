import Image from "next/image";
import TCRImage from "@/assets/img/Logo TCR.png";
import Playstore from "@/assets/img/googleplay.png";
import Appstore from "@/assets/img/appstore.png";

export default function DetailBanner() {
 return (
  <div className='flex flex-col-reverse lg:flex-row items-center gap-14 justify-center w-8/12 mx-auto my-20'>
   <div>
    <Image
     src={TCRImage}
     alt='detail banner'
     className='mb-4 w-[200px] mx-auto lg:mx-0 lg:w-[400px]'
    />
    <div className='flex gap-4'>
     <Image
      src={Playstore}
      alt='Playstore'
      className='mb-4 lg:w-[200px] cursor-pointer'
     />
     <Image
      src={Appstore}
      alt='App store'
      className='mb-4 lg:w-[200px] cursor-pointer'
     />
    </div>
   </div>
   <div>
    <h1 className='font-bold antialiased text-3xl mb-4'>Temen Curhat Rohani</h1>
    <p className='text-lg'>
     Aplikasi Teman Curhat Rohani dirancang oleh Savin Solution sebagai platform
     anak-anak muda maupun dewasa Kristiani yang memiliki keluh kesah dalam
     kehidupan sehari-hari, namun tidak mempunyai tempat untuk bercerita
    </p>
    <br />
    <p className='text-lg'>
     Aplikasi ini diharapkan dapat menjadi sebuah bentuk dukungan emosional
     dengan kata-kata semangat melalui ayat-ayat yang berhubungan dengan masalah
     yang sedang mereka hadapi.
    </p>
   </div>
  </div>
 );
}
