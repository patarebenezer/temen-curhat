import Image from "next/image";
import LogoImage from "@/assets/img/logo.png";

export default function Logo() {
 return <Image src={LogoImage} alt='logo TCR' />;
}
