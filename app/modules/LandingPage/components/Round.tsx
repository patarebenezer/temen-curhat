type RoundProps = {
 type: string;
};

export default function Round({ type }: RoundProps) {
 return (
  <div
   className={`bg-yellow-500 absolute ${
    type === "Top"
     ? "top-0 right-0 rounded-bl-full p-40"
     : "bottom-0 left-0 rounded-tr-full p-32"
   } overflow-hidden`}
  ></div>
 );
}
