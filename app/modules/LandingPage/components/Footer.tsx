import React from "react";
import Logo from "./Logo";
import Menu from "../Menu";

export default function Footer() {
 return (
  <div className='py-5 px-12 gap-5 w-full justify-start flex bg-gradient-to-br from-black to-gray-700 h-1/5'>
   <div className='my-5'>
    <Logo />
   </div>
   <Menu type='Footer' />
  </div>
 );
}
