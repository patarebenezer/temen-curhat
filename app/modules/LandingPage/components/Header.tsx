import Menu from "../Menu";
import Logo from "./Logo";

export default function Header() {
 return (
  <div className='flex justify-between items-center py-3 px-5 lg:px-12'>
   <Logo />
   <div className='flex gap-6 items-center'>
    <div className='hidden lg:block'>
     <Menu />
    </div>
    <button className=' bg-yellow-500 rounded-full font-semibold antialiased hover:bg-yellow-400 px-2 py-1 transition duration-300 ease-in-out'>
     contact us
    </button>
   </div>
  </div>
 );
}
